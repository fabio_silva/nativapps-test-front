Para rodar: ng server --open
já irá abrir o browser

Estou usando Sass / Scss. Também estou usando forms reativos.

Descomentado linhas em polyfills.ts para browser IE.

Estou usando como lib primeng e jquery.

No environment.js está configurado a url da api.

Usei um modelo de layout Metronic.

Cada entidade (aluno, curso, professor) tem um service que estende de EntidadeService que faz todo o serviço (get all - mantendo em cache, get by id, save e delete) usando Generics. Cada listagem só preciso configurar o nome das colunas (coleção) que o GridDefault monta as colunas, faz o get all, delete, e as popula e no cadastro configuro o nome das colunas (coleção) e o html é criado dinamicamente lendo dessa coleção e o FormDefault faz o processo de get by id e save.

Nos environments deixei configurado o link da api, sendo injetado nas classes do projeto e podendo ser configurado para ter valores diferentes para os profiles dev e prod.

Estou colocando o foco nos campos principais ao abrir a tela.

Meu Perfil e Alterar Senha só deixei as opções no menu.

FALTOU: Requisição para o server de login, delete e validações