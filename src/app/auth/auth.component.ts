import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";

import { AppService } from './../services/app.service';
import { AuthService } from "./../services/auth.service";
import { Helpers } from './../helpers';
import { LoginCustom } from "./login-custom";
import { ScriptLoaderService } from './../services/script-loader.service';

@Component({
    selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
    templateUrl: './auth.component.html',
    encapsulation: ViewEncapsulation.None
})

export class AuthComponent implements OnInit {
    returnUrl: string;

    constructor(private _router: Router,
        private _script: ScriptLoaderService,
        private _route: ActivatedRoute,
        private _authService: AuthService,
        private _appService: AppService) {
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'];
        if (this.returnUrl) this._router.navigate([this.returnUrl]);

        this._script.load('body', 'assets/vendors/base/vendors.bundle.js', 'assets/js/scripts.bundle.js')
            .then(() => {
                Helpers.setLoading(false);
                LoginCustom.init();

                this._appService.focus($("#login"));
            });
    }

    signin() {
        this._router.navigate(['/dashboard']);
        /*
        this.loading = true;
        this._authService.login(this.model.email, this.model.password)
            .subscribe(
            data => {
                this._router.navigate([this.returnUrl]);
            },
            error => {
                this.showAlert('alertSignin');
                this._alertService.error(error);
                this.loading = false;
            });
            */
    }

    forgotPass() {
        /*
        this._userService.forgotPassword(this.model.email)
            .subscribe(
            data => {
                this.showAlert('alertSignin');
                this._alertService.success('Cool! Password recovery instruction has been sent to your email.', true);
                this.loading = false;
                LoginCustom.displaySignInForm();
                this.model = {};
            },
            error => {
                this.showAlert('alertForgotPass');
                this._alertService.error(error);
                this.loading = false;
            });
            */
    }
}