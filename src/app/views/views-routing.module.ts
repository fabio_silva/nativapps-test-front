import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlunoComponent } from './alunos/aluno.component';
import { AlunosComponent } from './alunos/alunos.component';
import { AuthGuard } from '../auth/auth.guard';
import { CursoComponent } from './cursos/curso.component';
import { CursosComponent } from './cursos/cursos.component';
import { DashboardComponent } from './dashboard.component';
import { LayoutComponent } from './../layout/layout.component';
import { ProfessorComponent } from './professores/professor.component';
import { ProfessoresComponent } from './professores/professores.component';

const routes: Routes = [
    {
        "path": "", "component": LayoutComponent, "canActivate": [ AuthGuard ], "canActivateChild": [ AuthGuard ],
            "children": [
                { "path": "aluno", component: AlunoComponent },  
                { "path": "aluno/:id", component: AlunoComponent },                
                { "path": "alunos", component: AlunosComponent },                
                { "path": "curso", component: CursoComponent },  
                { "path": "curso/:id", component: CursoComponent },                
                { "path": "cursos", component: CursosComponent },   
                { "path": "dashboard", component: DashboardComponent },
                { "path": "professor", component: ProfessorComponent },  
                { "path": "professor/:id", component: ProfessorComponent },                
                { "path": "professores", component: ProfessoresComponent }                
            ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ViewsRoutingModule { }