import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    template: `
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
        </div>
    </div>    
    `,
    encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent {

    constructor() { }
}