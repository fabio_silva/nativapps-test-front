import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { AppService } from './../../services/app.service';
import { CursoService } from './curso.service';
import { FormDefault } from '../../componentes/form-default';
import { IEntidadeService } from './../../services/ientidade-service';

@Component({
  selector: 'app-curso',
  templateUrl: './curso.component.html',
  encapsulation: ViewEncapsulation.None
})
export class CursoComponent extends FormDefault<any> implements OnInit {

  fields = [
    { name: 'codigo', type: 'text', label: 'Código' },
    { name: 'nome', type: 'text', label: 'Nome' },
    { name: 'observacoes', type: 'text', label: 'Observações' }
  ];

  constructor(_service: CursoService,
              _appService: AppService,
              _route: ActivatedRoute,
              _router: Router,
              _formBuilder: FormBuilder) { 

    super(_service, _appService, _route, _router, _formBuilder);
  }

  ngOnInit() {
    this.init(this.fields);
  }

  protected initForm() {
    super.initForm();

    setTimeout(() => this._appService.focus($("#codigo")), 100);
  }    
}
