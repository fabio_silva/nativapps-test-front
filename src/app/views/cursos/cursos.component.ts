import { Component, ViewEncapsulation } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { CursoService } from './curso.service';
import { GridDefault } from './../../componentes/grid/grid-default';

@Component({
  selector: 'app-cursos',
  template: `<app-grid-default [colunas]="colunas" [dados]="dados" [options]="options" (excluir)="excluir($event)"></app-grid-default>`,
  encapsulation: ViewEncapsulation.None
})
export class CursosComponent extends GridDefault<any> {

  constructor(private _service: CursoService) { 

    super(_service);

    this.prepareColunas(
        [
          { field: "id", title: "#" }, 
          { field: "codigo", title: "Código" },
          { field: "nome", title: "Nome" }        
        ] 
    );
  }

  excluir(id: any) {
    console.log(id);
  }  
}
