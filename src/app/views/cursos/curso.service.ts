import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

import { EntidadeService } from '../../services/entidade-service';

@Injectable()
export class CursoService extends EntidadeService<any> {
  readonly route: string = 'curso';
  readonly routeListar: string = 'cursos';

  constructor(_http: HttpClient) { 
    super(_http);
    this._url += 'cursos';
  }
}
