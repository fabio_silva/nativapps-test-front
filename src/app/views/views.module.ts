import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { DropdownModule } from 'primeng/dropdown';

import { AuthGuard } from '../auth/auth.guard';
import { AlunoComponent } from './alunos/aluno.component';
import { AlunosComponent } from './alunos/alunos.component';
import { AlunoService } from './alunos/aluno.service';
import { ComponentesModule } from '../componentes/componentes.module';
import { CursoComponent } from './cursos/curso.component';
import { CursosComponent } from './cursos/cursos.component';
import { CursoService } from './cursos/curso.service';
import { DashboardComponent } from './dashboard.component';
import { DirectivesModule } from './../directives/directives.module';
import { LayoutModule } from '../layout/layout.module';
import { ProfessorComponent } from './professores/professor.component';
import { ProfessoresComponent } from './professores/professores.component';
import { ProfessorService } from './professores/professor.service';
import { ViewsRoutingModule } from './views-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ComponentesModule,
        DirectivesModule,
        DropdownModule,
        FormsModule,
        LayoutModule,
        ReactiveFormsModule,
        ViewsRoutingModule
    ],
    declarations: [
        AlunoComponent,
        AlunosComponent,
        CursoComponent,
        CursosComponent,
        DashboardComponent,
        ProfessorComponent,
        ProfessoresComponent,
    ],
    providers: [
        AlunoService,
        AuthGuard,
        CursoService,
        ProfessorService,
    ]
})
export class ViewsModule { }
