import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

import { EntidadeService } from '../../services/entidade-service';

@Injectable()
export class ProfessorService extends EntidadeService<any> {
  readonly route: string = 'professor';
  readonly routeListar: string = 'professores';

  constructor(_http: HttpClient) { 
    super(_http);
    this._url += 'professores';
  }
}
