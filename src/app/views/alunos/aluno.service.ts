import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

import { EntidadeService } from '../../services/entidade-service';

@Injectable()
export class AlunoService extends EntidadeService<any> {
  readonly route: string = 'aluno';
  readonly routeListar: string = 'alunos';

  constructor(_http: HttpClient) { 
    super(_http);
    this._url += 'alunos';
  }
}
