import { Component, ViewEncapsulation } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { AlunoService } from './aluno.service';
import { GridDefault } from './../../componentes/grid/grid-default';

@Component({
  selector: 'app-alunos',
  template: `<app-grid-default [colunas]="colunas" [dados]="dados" [options]="options" (excluir)="excluir($event)"></app-grid-default>`,
  encapsulation: ViewEncapsulation.None
})
export class AlunosComponent extends GridDefault<any> {

  constructor(private _service: AlunoService) { 

    super(_service);

    this.prepareColunas(
        [
          { field: "id", title: "#" }, 
          { field: "identificacao", title: "Identificação" },
          { field: "nome", title: "Nome" },
          { field: "sobrenome", title: "Sobrenome" },
          { field: "genero", title: "Gênero" },          
        ] 
    );
  }

  excluir(id: any) {
    console.log(id);
  }
}
