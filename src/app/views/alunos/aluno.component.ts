import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { AlunoService } from './aluno.service';
import { AppService } from './../../services/app.service';
import { FormDefault } from '../../componentes/form-default';
import { IEntidadeService } from './../../services/ientidade-service';

@Component({
  selector: 'app-aluno',
  templateUrl: './aluno.component.html',
  encapsulation: ViewEncapsulation.None
})
export class AlunoComponent extends FormDefault<any> implements OnInit {

  fields = [
    { name: 'identificacao', type: 'text', label: 'Identificação' },
    { name: 'nome', type: 'text', label: 'Nome' },
    { name: 'sobrenome', type: 'text', label: 'Sobrenome' },
    { name: 'genero', type: 'text', label: 'Gênero' }            
  ];

  constructor(_service: AlunoService,
              _appService: AppService,
              _route: ActivatedRoute,
              _router: Router,
              _formBuilder: FormBuilder) { 

    super(_service, _appService, _route, _router, _formBuilder);
  }

  ngOnInit() {
    this.init(this.fields);
  }
}
