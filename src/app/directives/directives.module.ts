import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { HrefPreventDefaultDirective } from './href-prevent-default.directive';
import { UnwrapTagDirective } from './unwrap-tag.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    HrefPreventDefaultDirective,
    UnwrapTagDirective    
  ],
  exports: [
    HrefPreventDefaultDirective,
    UnwrapTagDirective    
  ]
})
export class DirectivesModule { }
