import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppService } from './services/app.service';
import { AuthComponent } from './auth/auth.component';
import { AuthService } from './services/auth.service';
import { ComponentesModule } from "./componentes/componentes.module";
import { DirectivesModule } from './directives/directives.module';
import { ScriptLoaderService } from "./services/script-loader.service";
import { ViewsModule } from "./views/views.module";

@NgModule({
    declarations: [
        AppComponent,
        AuthComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserAnimationsModule,
        BrowserModule,
        ComponentesModule,
        DirectivesModule,
        HttpClientModule,
        RouterModule,
        ViewsModule
    ],
    providers: [
        AppService,
        AuthService,
        ScriptLoaderService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { 
    constructor(private _appService: AppService) {
        this._appService.config();
    }    
}