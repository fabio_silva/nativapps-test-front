import { environment } from "../../environments/environment";
import { EventEmitter } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, Observer } from "rxjs/Rx";

import { Helpers } from "./../helpers";
import { IEntidadeService } from "./ientidade-service";

export abstract class EntidadeService<T> implements IEntidadeService {
    protected _entidades: Array<T>;
    protected get entidades(): Array<T> {
        return this._entidades;
    }
    protected set entidades(value: Array<T>) {
        this._entidades = value;
        this.entidadesChanged.next(true);
    }
    protected entidadesChanged: EventEmitter<boolean> = new EventEmitter<boolean>();
    readonly route: string;
    readonly routeListar: string;    
    protected _url: string = `${environment.apiLink}`;

    constructor(protected _http: HttpClient) { }

    getAll(): Observable<Array<T>> {
        Helpers.setLoading(true);
        let observable: Observable<Array<T>> = Observable.create((observer: Observer<Array<T>>) => {
            if (this.entidades) {
              observer.next(this.entidades);
              observer.complete();
              Helpers.setLoading(false);
            } else {
              Helpers.setLoading(true);
              this._http.get<Array<T>>(this._url).subscribe((data: Array<T>) => {
                if (!data) data = [];
                this.entidades = data;
                observer.next(this.entidades);
                observer.complete();
                Helpers.setLoading(false);
              });
            }
        });
        return observable;
    }

    getById(id: number): Observable<T> {
        let observable: Observable<T> = Observable.create((observer: Observer<T>) => {
            if (id === undefined || id <= 0) {
                observer.next(null);
                observer.complete();    
                return;
            }

            this.getAll().subscribe((entidades: Array<T>) => {
                if (!entidades) return;
                let entidade = entidades.find(item => item['id'] == id);
                if (!entidade) return;
                if (this.isCached(entidade)) {
                    observer.next(entidade);
                    observer.complete();                
                } else {
                    Helpers.setLoading(true);
                    this._http.get(`${this._url}/${id}`).subscribe((entidadeServer: T) => {
                        Object.assign(entidade, entidadeServer);
                        observer.next(entidadeServer);
                        observer.complete();    
                        Helpers.setLoading(false);            
                    })
                }
            });
        });
        return observable;      
    }

    protected isCached(entidade: T): boolean {
        return true;
    }

    salvar(entidade: T): Observable<boolean> {
        Helpers.setLoading(true);
    
        let observable: Observable<boolean> = Observable.create((observer: Observer<boolean>) => {  
            if (entidade['id']) {
                this._http.put(this._url, entidade).subscribe(() => {
                    this.getById(entidade['id']).subscribe((entidadeCached: T) => {
                        Object.assign(entidadeCached, entidade);
                        observer.next(true);
                        observer.complete();                     
                        Helpers.setLoading(false);
                    });
                });
            } else {
                this._http.post(this._url, entidade).subscribe(data => {
                    entidade['id'] = parseInt(data.toString());
                    this.entidades.push(entidade);
                    observer.next(true);
                    observer.complete();                     
                    Helpers.setLoading(false);
                });
            }
        });
    
        return observable;
    }    

    add(entidade: T) {
        this.entidades.push(entidade);
    }
}
