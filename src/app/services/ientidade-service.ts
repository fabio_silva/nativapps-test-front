import { Observable, Observer } from "rxjs/Rx";

export interface IEntidadeService {
    getById(id: number): Observable<any>;
}
