import { Injectable } from '@angular/core';

import { AuthService } from './auth.service';

import $ from 'jquery/dist/jquery';

@Injectable()
export class AppService {

  static localStorage = new function() {
    this.JSONtoString = function(object: any) {
        return JSON.stringify(object);
    };

    this.save = function(name: string, data: any){
        localStorage.setItem(name, this.JSONtoString(data));
    };

    this.get = function(name: string) {
        const item = localStorage.getItem(name);
        return JSON.parse(item);
    };

    this.remove = function(name: string) {
        localStorage.removeItem(name);
    };
  };

  constructor(private _authService: AuthService) { }

  config() {
  }      

  cursorToEnd(input) {
    input.focus();

    // If this function exists...
    if (input.setSelectionRange) {
        // ... then use it (Doesn't work in IE)
        // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
        const len = $(input).val().length * 2;

        input.setSelectionRange(len, len);
    } else {
        // ... otherwise replace the contents with itself
        // (Doesn't work in Google Chrome)
        $(input).val($(input).val());
    }

    // Scroll to the bottom, in case we're in a tall textarea
    // (Necessary for Firefox and Google Chrome)
    input.scrollTop = 999999;
  }

  focus(jQueryObject) {
    setTimeout(() => this.cursorToEnd(jQueryObject), 300);
  };  
}
