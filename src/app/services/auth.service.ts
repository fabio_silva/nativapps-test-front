import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Observer } from "rxjs/Rx";
import { Router } from "@angular/router";

import { Helpers } from "./../helpers";

@Injectable()
export class AuthService {

  constructor(private _http: HttpClient, 
              private _router: Router) { }












  isAuthenticated(): boolean {
    return true;
  }

  forget() {}

  login(email: string, password: string) {
    /*
        return this.http.post('/api/authenticate', JSON.stringify({ email: email, password: password }))
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }
            });
            */
  }

  logout() {
    Helpers.setLoading(true);
    // remove user from local storage to log user out
    localStorage.removeItem("currentUser");
    this._router.navigate(["/login"]);
  }
}
