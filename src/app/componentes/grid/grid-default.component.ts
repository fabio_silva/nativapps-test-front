import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

import { EntidadeService } from '../../services/entidade-service';
import { IGridOptions } from './grid-default';

@Component({
  selector: 'app-grid-default',
  templateUrl: './grid-default.component.html',
  encapsulation: ViewEncapsulation.None
})
export class GridDefaultComponent {

  @Input() 
  colunas: Array<any>;

  @Input() 
  dados: Array<any>;

  @Output()
  excluir: EventEmitter<any> = new EventEmitter<any>();

  private _options: IGridOptions;
  get options(): IGridOptions {
    return this._options;
  }
  @Input() 
  set options(value: IGridOptions) {
    this._options = value;  
  }

  constructor() { }

  excluirDispatch(info: any) {
    this.excluir.emit(info);
  }
}
