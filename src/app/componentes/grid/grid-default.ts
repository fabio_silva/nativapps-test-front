import { EntidadeService } from "../../services/entidade-service";

export abstract class GridDefault<T> {

    colunas: Array<any>;
    dados: Array<T>;
    options: IGridOptions;

    constructor(protected _entidadeService: EntidadeService<T>) { 
        this._entidadeService.getAll().subscribe( data => this.dados = data );           

        this.options = { route: _entidadeService.route };   
    }   

    protected prepareColunas(colunas: Array<any>) {
        colunas.push({ field: "editar", title: "Editar", sortable: false });
        colunas.push({ field: "excluir", title: "Excluir", sortable: false }); 

        colunas.forEach( item => {
          if (item.title) {
              if (item.sortable === undefined) item.sortable = true;
          }
        });
  
        this.colunas = colunas;
    } 
}

export interface IGridOptions {
    route: string;
}
