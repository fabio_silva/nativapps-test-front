import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TableModule } from 'primeng/table';

import { GridDefaultComponent } from './grid/grid-default.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        TableModule
    ],
    declarations: [
        GridDefaultComponent
    ],
    exports:[
        GridDefaultComponent
    ]
})
export class ComponentesModule { }
