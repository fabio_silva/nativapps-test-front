import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { AppService } from './../services/app.service';
import { EntidadeService } from '../services/entidade-service';

export abstract class FormDefault<T> {
    
    protected _fields: Array<any>;
    form: FormGroup;    
    protected id: number;
    isAdd: boolean;

    constructor(protected _service: EntidadeService<T>,
                protected _appService: AppService,
                protected _route: ActivatedRoute,
                protected _router: Router,
                protected _formBuilder: FormBuilder) { }

    cancelar() {
        const routeListar: string = this._service.routeListar;
        if (routeListar) {
            this._router.navigate([`/${routeListar}`]);
        }
    }

    protected init(fields: Array<any>) {
        this._fields = fields;

        this._route.params.subscribe(params => {
            this.isAdd = !params.hasOwnProperty('id');
            if (!this.isAdd) this.id = params.id;
            this.initForm();
        });     
    }

    protected initForm() {
        let formGroup: any = { id: [ null ] };
        this._fields.map( item => formGroup[item.name] = [ null ] );
    
        this.form = this._formBuilder.group(formGroup);     
    
        setTimeout(() => this._appService.focus($("#identificacao")), 100);
    
        if (!this.isAdd) {
          this._service.getById(this.id).subscribe((item: any) => {
            this.form.patchValue(item);
          })    
        }        
    }

    salvar() {
        let toSave = this.form.value;
        this._service.salvar(toSave).subscribe(() => this.cancelar());        
    }
}
