import { AfterViewInit, Component, ViewEncapsulation } from "@angular/core";

import { AuthService } from "../services/auth.service";

declare let mLayout: any;
@Component({
  selector: "app-header-nav",
  templateUrl: "./header-nav.component.html",
  encapsulation: ViewEncapsulation.None
})
export class HeaderNavComponent implements AfterViewInit {

  userOptions: Array<any> = [
    { opcao: "Meu Perfil", icon: "flaticon-profile-1", link: "perfil" },
    { opcao: "Alterar Senha", icon: "flaticon-refresh", link: "alterar-senha" }
  ];

  options: Array<any> = [
    { opcao: "Alunos", link: "alunos" },
    { opcao: "Cursos", link: "cursos" },
    { opcao: "Professores", link: "professores" }
  ];

  constructor(private _authService: AuthService) { }

  ngAfterViewInit() {
    mLayout.initHeader();
  }

  logout() {
    this._authService.logout();
  }
}
