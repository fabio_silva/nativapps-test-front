import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer.component';
import { HeaderNavComponent } from './header-nav.component';
import { LayoutComponent } from './layout.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [
        FooterComponent,
        HeaderNavComponent,
        LayoutComponent
    ]
})
export class LayoutModule { }
